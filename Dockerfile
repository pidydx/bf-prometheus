#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=prometheus
ENV APP_GROUP=prometheus

ENV NODE_EXPORTER_VERSION="1.8.2"
ENV ALERTMGR_VERSION="0.27.0"
ENV SERVER_VERSION="2.54.0"

# Update users and groups
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -s /usr/sbin/nologin

# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*

##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV BUILD_DEPS wget

# Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS} \
 && rm -rf /var/lib/apt/lists/*

# Run build
WORKDIR /usr/src
RUN wget -nv https://github.com/prometheus/alertmanager/releases/download/v${ALERTMGR_VERSION}/alertmanager-${ALERTMGR_VERSION}.linux-amd64.tar.gz
RUN tar -zxf alertmanager-${ALERTMGR_VERSION}.linux-amd64.tar.gz -C /usr/local/bin --strip-components=1
RUN wget -nv https://github.com/prometheus/node_exporter/releases/download/v${NODE_EXPORTER_VERSION}/node_exporter-${NODE_EXPORTER_VERSION}.linux-amd64.tar.gz
RUN tar -zxf node_exporter-${NODE_EXPORTER_VERSION}.linux-amd64.tar.gz -C /usr/local/bin --strip-components=1
RUN wget -nv https://github.com/prometheus/prometheus/releases/download/v${SERVER_VERSION}/prometheus-${SERVER_VERSION}.linux-amd64.tar.gz
RUN tar -zxf prometheus-${SERVER_VERSION}.linux-amd64.tar.gz -C /usr/local/bin --strip-components=1
RUN rm /usr/local/bin/LICENSE
RUN rm /usr/local/bin/NOTICE
RUN rm /usr/local/bin/*.yml


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

RUN mkdir /host \
 && mkdir /var/lib/prometheus \
 && chown -R ${APP_USER} /var/lib/prometheus

EXPOSE 9090/tcp 9094/tcp 9100/tcp
VOLUME ["/etc/prometheus", "/var/lib/prometheus", "/host/sys", "/host/root" ]

USER ${APP_USER}
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["server"]
