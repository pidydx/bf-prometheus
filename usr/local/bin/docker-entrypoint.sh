#!/bin/bash

set -e

if [ "$1" = 'alertmgr' ]; then
    exec alertmanager --config.file=/etc/prometheus/alertmanager.yml
fi

if [ "$1" = 'server' ]; then
    exec prometheus --config.file=/etc/prometheus/prometheus.yml --storage.tsdb.path=/var/lib/prometheus
fi

exec "$@"
